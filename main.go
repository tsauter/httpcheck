package main

import (
	"checkmk"
	"errors"
	"flag"
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

type Config struct {
	Debug       bool  `yaml:"Debug"`
	Concurrency int   `yaml:"Concurrency"`
	Urls        []Url `yaml:"Urls"`
}

type Url struct {
	URI                 string `yaml:"URI"`
	UseTLS              bool   `yaml:"UseTLS"`
	ExpectedStatusCodes []int  `yaml:"ExpectedStatusCodes"`
	SearchRegex         string `yaml:"SearchRegex"`
}

var (
	cmk checkmk.CheckMKStatus
	cfg Config

	netTransport *http.Transport

	Version   = "xx"
	BuildTime = "xx"
	BuildUser = "xx"
	BuildHost = "xx"
)

func loadConfig() error {
	mydir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return err
	}

	cfgfilename := filepath.Join(mydir, "..", "config", "httpcheck.yaml")
	content, err := ioutil.ReadFile(cfgfilename)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(content, &cfg); err != nil {
		return err
	}

	if cfg.Concurrency < 1 {
		cfg.Concurrency = 1
	}

	flag.IntVar(&cfg.Concurrency, "concurrency", cfg.Concurrency, "number of concurrent downloads")
	flag.BoolVar(&cfg.Debug, "debug", cfg.Debug, "activate additional debug output")
	showVersion := flag.Bool("version", false, "show version and exit")
	flag.Parse()

	if *showVersion {
		fmt.Printf("httpcheck: v%s (builded at %s on %s@%s)\n", Version,
			BuildTime, BuildUser, BuildHost)
		os.Exit(1)
	}

	return nil
}

func main() {
	cmk = checkmk.CheckMKStatus{PluginName: "httpcheck"}

	if err := loadConfig(); err != nil {
		WriteError(err.Error())
		os.Exit(1)
	}

	if cfg.Concurrency < 1 {
		WriteError("A minimum of 1 concurrent workers is required.")
		os.Exit(1)
	}

	netTransport = &http.Transport{
		Dial:                (&net.Dialer{Timeout: 5 * time.Second}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	// create a channel with a maximum of concurrency entries
	// as soon as the channel is full, the next go routine will blocked
	sem := make(chan bool, cfg.Concurrency)

	for _, url := range cfg.Urls {
		// add an entry to the channel
		sem <- true

		go func(u Url) {
			// make sure, we free up the channel, regarless what happens
			defer func() { <-sem }()

			// validate the url
			ValidateUrl(&u)
		}(url)
	}

	// Try to fill in the channel, as soon as we have filled up
	// everything no other routines are running any more
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}

	os.Exit(0)
}

func IsStatusCodeOk(code int, list []int) bool {
	for _, v := range list {
		if v == code {
			return true
		}
	}
	return false
}

func ValidateUrl(url *Url) error {
	if cfg.Debug {
		fmt.Printf("# %#v\n", url)
	}

	// Set safe timeout values
	// https://medium.com/@nate510/don-t-use-go-s-default-http-client-4804cb19f779#.z3nocclxz
	var netClient = &http.Client{
		Timeout:   time.Second * 30,
		Transport: netTransport,
	}

	startTime := time.Now()

	// Connect to the specfied URL and fetch the complete site
	// When the fetch failed, report an error to check_mk
	response, err := netClient.Get(url.URI)
	if err != nil {
		elapsed := time.Since(startTime)
		WriteResponse(url, response.StatusCode, elapsed, 0, err.Error())
		return err
	}
	defer response.Body.Close()

	size, err := ValidateResponse(url, response)
	if err != nil {
		elapsed := time.Since(startTime)
		WriteResponse(url, response.StatusCode, elapsed, size, err.Error())
		return err
	}

	elapsed := time.Since(startTime)

	// When the fetch was successfull, report them to check_mk
	if cfg.Debug {
		fmt.Printf("# %#v\n", response)
	}
	WriteResponse(
		url,
		response.StatusCode,
		elapsed,
		size,
		response.Status,
	)

	return nil
}

func ValidateResponse(url *Url, response *http.Response) (int, error) {
	// Validate the status code of the http response
	// When a list of expected codes is configured, validate
	// against these list entries.
	// In case of an empty list, 2xx return codes are exected
	if len(url.ExpectedStatusCodes) > 0 {
		if !IsStatusCodeOk(response.StatusCode, url.ExpectedStatusCodes) {
			return 0, fmt.Errorf("%d %s (unexpected)", response.StatusCode, http.StatusText(response.StatusCode))
			//return 0, errors.New("Invalid response code")
		}
	} else {
		if (response.StatusCode < 200) || (response.StatusCode > 299) {
			return 0, fmt.Errorf("%d %s", response.StatusCode, http.StatusText(response.StatusCode))
			//return 0, errors.New("Non 2xx response code")
		}
	}

	// read the whole body
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return len(body), err
	}
	if cfg.Debug {
		fmt.Printf("# %s\n", body)
	}

	// validate the body against a possible existing regex
	if url.SearchRegex != "" {
		pattern, err := regexp.Compile(url.SearchRegex)
		if err != nil {
			return len(body), err
		}

		if !pattern.Match(body) {
			return len(body), errors.New("Search pattern not found in http response.")
		}
	}

	return len(body), nil
}

func WriteResponse(url *Url, returnCode int, duration time.Duration, size int, message string) {
	line := fmt.Sprintf("%s\t%d\t%d\t%d\t%s", url.URI, returnCode, duration/time.Millisecond, size, message)
	cmk.ReportLine(line)
}

func WriteError(message string) {
	line := fmt.Sprintf("%s\t%d\t%d\t%d\t%s", "UNKNOWN", 1, 0, 0, message)
	cmk.ReportLine(line)
}
