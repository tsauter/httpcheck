package main

import (
	"errors"
	"net"
	"net/http"
	"testing"
	"time"
)

/*
	URI                 string `yaml:"URI"`
	UseTLS              bool   `yaml:"UseTLS"`
	ExpectedStatusCodes []int  `yaml:"ExpectedStatusCodes"`
	SearchRegex         string `yaml:"SearchRegex"`
*/

func TestValidateResponse(t *testing.T) {

	fs := http.FileServer(http.Dir("test-fixtures"))
	http.Handle("/", fs)
	go func() {
		http.ListenAndServe("127.0.0.1:9999", nil)
	}()

	testcases := []struct {
		name          string
		url           Url
		expectedError error
	}{
		{
			"sample1",
			Url{
				URI: "http://localhost:9999/sample1.html",
			},
			nil,
		},
		{
			"sample1.1",
			Url{
				URI: "http://localhost:9999/sample1-notthere.html",
			},
			errors.New("Non 2xx response code"),
		},
		{
			"sample1.2",
			Url{
				URI:         "http://localhost:9999/sample1.html",
				SearchRegex: ".*noop.*",
			},
			nil,
		},
	}

	// configure the netTransport variable, otherwise we get a null point exception
	netTransport = &http.Transport{
		Dial:                (&net.Dialer{Timeout: 5 * time.Second}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	for _, testdata := range testcases {

		// Validate the respons in the testcase
		err := ValidateUrl(&testdata.url)
		if (err == nil) && (testdata.expectedError == nil) {
			// not an error
			continue
		}

		// error is empty, but we expect an error
		if (err == nil) && (testdata.expectedError != nil) {
			t.Errorf("[%s] Expected error from ValidateUrl() not returned", testdata.name)
			continue
		}

		// ValidateUrl() returns an error, but we don't expect one
		if (err != nil) && (testdata.expectedError == nil) {
			t.Errorf("[%s] Unexpected error from ValidateUrl(): %s", testdata.name, err.Error())
			continue
		}

		// error is returned and we expect an error
		if err.Error() != testdata.expectedError.Error() {
			t.Errorf("[%s] Return error missmatch: %s <> %s", testdata.name, err.Error(), testdata.expectedError.Error())
			continue
		}

	}

}
